class User < ActiveRecord::Base
	validates :First_name, presence: true, length: {minimum: 3, maximum: 30}
	validates :Last_name, presence: true, length: {minimum: 3, maximum: 30}
	validates :Email, presence: true, length: {minimum: 10, maximum: 50}
	
end